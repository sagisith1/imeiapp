import { Component } from '@angular/core';
import { ConfigService } from '../services/config.service';
import { Router } from '@angular/router';
import { CommunicationService } from '../services/communication.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  userName = '';
  imei = '';
  infoData = {
    lat: '',
    lng: ''
  };

  constructor( 
    private _config: ConfigService, 
    private _router: Router, 
    private _comm: CommunicationService ) {
    //this._config.getUserName().subscribe(data => this.userName = data);
  }

  ngOnInit() {
    this._config.getUserName().subscribe((data) => {
      this.userName = data;
    });

    this._comm.lastSendRequestInfo$.subscribe((data) => {
      
      if ( typeof data['coords'] !== 'undefined') {
        this.infoData.lat = data['coords']['latitude'];
        this.infoData.lng = data['coords']['longitude'];
      }
    });

    this._comm.obsUser$.subscribe((data) => {
      this.imei = data;
    });

    this._config.emmitValues();
  }

  saveName() {
    if (this.userName !== '') {
      this._router.navigate(['tabs/tab1']);
      this._config.changeUserName(this.userName);
    }
    
  }


}
