import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private activateService = true;
  private userName = '';

  private obsActivated$: Subject<boolean>;
  private obsUserName$: Subject<string>;

  constructor(private _nativeStorage: NativeStorage) {
    this.obsActivated$ = new Subject();
    this.obsActivated$.next(this.activateService);
    this.obsUserName$ = new Subject();
    this.obsUserName$.next(this.userName);

    this._nativeStorage.getItem('storage_username')
    .then(
      data => this.changeUserName(data),
      error => console.error(error)
    );

   }

  getActivateService(): Observable<boolean> {
    return this.obsActivated$;
  }

  changeActivateService() {
    this.activateService = !this.activateService;
    this.obsActivated$.next(this.activateService);
  }

  getUserName(): Observable<string> {
    return this.obsUserName$;
  }

  changeUserName(newName: string) {
    this.userName = newName;
    this._nativeStorage.setItem('storage_username', this.userName)
    .then(
      () => console.log('Storage save'),
      error => console.error(error)
    );
    this.obsActivated$.next(this.activateService);
    this.obsUserName$.next(this.userName);
  }

  emmitValues() {
    this.obsActivated$.next(this.activateService);
    this.obsUserName$.next(this.userName);
  }
}
