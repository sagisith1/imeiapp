import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Uid } from '@ionic-native/uid/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ConfigService } from './config.service';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Place } from '../models/place.model';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  public domain = 'https://wolfsandbox.com'

  public obsUser$: Subject<any>;
  public obsImei$: Subject<any>;
  public obsCoords$: Subject<any>;
  public lastSendRequestInfo$: Subject<any>;
  public listPlaces$: Subject<any>;

  public testObs$: Subject<any>;

  private imei;
  private username;
  public coords;
  private activated = false;

  

  constructor(private _uid: Uid, private androidPermissions: AndroidPermissions,
              private _config: ConfigService, private platform: Platform,
              private _http: HttpClient, private geolocation: Geolocation
              ) {
    this.obsUser$ = new Subject();
    this.obsImei$ = new Subject();
    this.obsCoords$ = new Subject();
    this.lastSendRequestInfo$ = new Subject();
    this.listPlaces$ = new Subject();
    this.testObs$ = new Subject();

    this.listPlaces$.next([]);

    this.coords = '';

    this._config.getUserName().subscribe( data => {
      this.username = data;
    });

    this._config.getActivateService().subscribe( data => {
      this.activated = data;
    });

    this.getGeoLoop();
    this.timerLoop();
  }


  getGeoLoop() {
  //this.getGeo();

  /*this.geolocation.watchPosition()
  .subscribe(position => {
    if (position.coords !== undefined) {
      let strCoords = position.coords.longitude + ' ' + position.coords.latitude;
      this.obsCoords$.next(strCoords);
    } else {
      this.obsCoords$.next(' ES UNDEFINED');
    }
  });*/

  this.geolocation.getCurrentPosition({enableHighAccuracy: true}).then((resp) => {
    //let strCoords = position.coords.longitude + ' ' + position.coords.latitude;
    
    this.coords = {latitude: resp.coords.latitude, longitude: resp.coords.longitude};
    this.obsCoords$.next(this.coords);
    this.testObs$.next(resp);

   }).catch((error) => {
     console.log('Error getting coords');
     this.testObs$.next(error);
     
  });



  /*this.geolocation.getCurrentPosition().then((resp) => {
    console.log(resp);
    this.coords = 'entra';
    //this.coords = resp.coords.latitude + ' / ' + resp.coords.longitude;
    // resp.coords.latitude
    // resp.coords.longitude
   }).catch((error) => {
      this.coords = JSON.stringify(error);
     //console.log('Error getting location', error);
   });*/
  }

  getImeiLoop() {
    this.platform.ready().then(() => {
    if (this.platform.is('cordova')) {

        /*this.imei = this._uid.IMEI;
        this.obsUser$.next(this.imei);*/
        this.getImei().then(data => {
            this.imei = data;
            this.obsUser$.next(this.imei);
        });

      } else {
        this.imei = 'estás en browser';
        this.obsUser$.next(this.imei);
      }


      if (this.activated) {
        this.sendData().subscribe( data => {
          if(data.error == null) {
            console.log(data.places);
            
            this.listPlaces$.next(data.places);
          }
          
        });
      }

    });
  }

  sendData() {

    let latitude = 'Not data latitude';
    let longitude = 'Not data longitude';
    if ( typeof this.coords !== 'undefined') {
      latitude = this.coords.latitude;
      longitude = this.coords.longitude;
    }

    let headers = new HttpHeaders();

    let params = new HttpParams();
    params = params.append('imei', this.imaiFake(this.imei));
    params = params.append('user', this.username);
    params = params.append('coords_latitude', this.coords.latitude);
    params = params.append('coords_longitude', this.coords.longitude);

    this.lastSendRequestInfo$.next({coords: this.coords, imei: this.imei});

    return this._http.post(
      this.domain + '/places/list-places.json',
      params, { headers: headers, withCredentials: false}
      ).pipe( map(dataRes => {
        
        
        let data = JSON.parse(dataRes['content']);

        console.log(data);
        console.log(dataRes);

        let results = [];

        data['places'].forEach(placeElem => {
          let place = new Place(placeElem);
          results.push(place);
        });

        console.log(results);
        
        
        return { error: data['error'], places: results};
    }) );
  }

  timerLoop() {
    setInterval(() => {
      this.getGeoLoop();
      this.getImeiLoop();
    }, 3000);
  }

  async getImei() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );
   
    if (!hasPermission) {
      
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );
      
      if (!result.hasPermission) {
        throw new Error('Permissions required');
      }
   
      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }
   
      
      
     return this._uid.IMEI;
   }

   /*async getGeo() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
    );
    
    //console.log("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
    
   
    if (!hasPermission) {
      
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
      );
   
      if (!result.hasPermission) {
        throw new Error('Permissions required');
      }
   
      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }
   
      
      
     return true;
   }*/


   operation(optionId: number) {
    let headers = new HttpHeaders();

    let params = new HttpParams();
    params = params.append('imei', this.imaiFake(this.imei));
    params = params.append('user', this.username);
    params = params.append('option_id', optionId.toString());
    params = params.append('coords_latitude', this.coords.latitude);
    params = params.append('coords_longitude', this.coords.longitude);

    return this._http.post(
      this.domain + '/listener/new-operation.json',
      params, { headers: headers, withCredentials: false}
      ).pipe( map(dataRes => {
        
        
        let data = JSON.parse(dataRes['content']);

        return { result: data['result']};
    }) );
  }

  imaiFake(imei) {
    let res = '';
    if (imei != null) {
      let tempres = Math.floor( (imei * 2) / 3 );
      res = tempres.toString();
    }

    return res;
  }
}
