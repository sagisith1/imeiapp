import { Component, OnInit, Input } from '@angular/core';
import { Place } from '../../../models/place.model';
import { ModalController } from '@ionic/angular';
import { Option } from '../../../models/option.model';
import { CommunicationService } from '../../../services/communication.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {

  @Input() place: Place = new Place();
  result: string = '';

  constructor(
    public modalController: ModalController,
    private _comm: CommunicationService
  ) { }

  ngOnInit() {}

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  clickOption(option: Option) {
    console.log(option);
    this._comm.operation(option.id).subscribe(res => {
      this.result = res.result;

      setTimeout(() => {
        this.result = '';
      }, 2000);
    });
  }
}
