import { Component } from '@angular/core';
import { ConfigService } from '../services/config.service';
import { Router } from '@angular/router';
import { CommunicationService } from '../services/communication.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Place } from '../models/place.model';
import { ModalController, Platform } from '@ionic/angular';
import { ModalComponent } from './components/modal/modal.component';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  ILatLng,
  Circle
} from '@ionic-native/google-maps/ngx';

interface circlePlace {
  circle: Circle;
  place_id: number;
  isInside: boolean;
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  map: GoogleMap;
  activated = true;
  username = '';
  imei = '';
  lastRequestInfo = '';
  selectedZone = 'map';
  marker: Marker = null;

  error = '';
  isError = false;
  testData = '';
  testData2 = '';

  latitude = null;
  longitude = null;

  listFinalPlaces: Place[] = [];
  listPlaces: Place[] = [];
  listCircles: circlePlace[] = [];

  constructor( 
    private _config: ConfigService,
    private router: Router,
    private _comm: CommunicationService ,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    public modalController: ModalController,
    private platform: Platform
  ) { 
    
  }

  ngOnInit() {
    
    this._comm.testObs$.subscribe(data => {
      this.testData2 = JSON.stringify( data);
    });

    this._config.getActivateService().subscribe((data) => {
      this.activated = data;
      if (this.activated) {
        document.getElementById('cstm-main-content').style.display = 'block';
      } else {
        document.getElementById('cstm-main-content').style.display = 'none';
      }
    });

    this._config.getUserName().subscribe((data) => {
      this.username = data;
      if (this.username !== '') {
        document.getElementById('cstm-main-content').style.display = 'block';
      }
    });

    this._comm.obsUser$.subscribe((data) => {
      this.imei = data;
    });

    this._comm.lastSendRequestInfo$.subscribe((data) => {
      this.lastRequestInfo = data;
      if ( typeof this.lastRequestInfo['coords'] !== 'undefined') {
        this.latitude = this.lastRequestInfo['coords']['latitude'];
        this.longitude = this.lastRequestInfo['coords']['longitude'];

        this.testData = this.latitude + ' <> ' + this.longitude;

        console.log(this.latitude);
        console.log(this.longitude);
        
        this.drawMapMarks();
      }
    });

    this._comm.listPlaces$.subscribe(data => {
      this.listPlaces = data;
    });

    /*this._comm.obsCoords$.subscribe(coors => {
      this.coordsText = 'eeee - ' + coors;
    });*/

    this._config.emmitValues();

    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  drawMapMarks() {

    //this.map.clear();
    
    let cameraPos: CameraPosition<ILatLng> = {
      target: {lat: this.latitude, lng: this.longitude},
      zoom: 18
    };
      
    this.map.moveCamera(cameraPos);
    
    
    //let newPos: ILatLng = {lat: this.latitude, lng: this.longitude};
    //this.marker.setPosition(newPos);
      
    if (this.marker != null) {
      this.marker.setPosition({
        lat: this.latitude,
        lng: this.longitude
      });
    }
    /*this.marker = this.map.addMarkerSync({
      title: 'Current position',
      icon: 'blue',
      //animation: 'DROP',
      position: {
        lat: this.latitude,
        lng: this.longitude
      }
    });*/


      /*

       listCircles.push({
          latitude: this.latitude,
          longitude: this.longitude,
          radius: 10
        });
      */

    this.listCircles.forEach(circlePlace => {
      circlePlace.circle.destroy();
    });

    this.listCircles = [];

    this.listPlaces.forEach(place => {
      if (place != null) {
        console.log(place);
  
        let center: ILatLng = {lat: place.lat, lng: place.lng};
        let tempCircle: Circle = this.map.addCircleSync({
          center,
          radius: place.radius,
          strokeColor: place.color,
          strokeWidth: 10,
          fillColor: place.color
        });
  
        let isInside = false;
        if (this.latitude != null && this.longitude != null){
          let currPosition: ILatLng = {lat: this.latitude, lng: this.longitude};
          isInside = tempCircle.getBounds().contains(currPosition);
        }
  
        place.isInside = isInside;
        this.listCircles.push({
          circle: tempCircle,
          place_id: place.id,
          isInside
        });
  
          
          /*this.listCircles.forEach(elemCirclePlace =>{
            let isInside = elemCircle.getBounds().contains({lat: place.lat, lng: place.lng});
            if(isInside){

            }
          });*/

      }
    });
    this.listFinalPlaces = JSON.parse(JSON.stringify(this.listPlaces));
    this.listFinalPlaces = this.listFinalPlaces.sort((obj1, obj2) => {
      if (obj1.isInside && !obj2.isInside) {
          return -1;
      }
  
      if (!obj1.isInside && obj2.isInside) {
          return 1;
      }
  
      return 0;
    });
    console.log(this.listCircles);
    console.log(this.listPlaces);
    
  }

  changeOnOff() {
    this._config.changeActivateService();
  }

  changeSelectedZone(zoneValue) {
    this.selectedZone = zoneValue.detail.value;
    if (this.selectedZone === 'map') {
      document.getElementById('sub_tab_places').style.display = 'none';
      document.getElementById('sub_tab_map').style.display = 'block';
    } else {
      document.getElementById('sub_tab_places').style.display = 'block';
      document.getElementById('sub_tab_map').style.display = 'none';
    }
  }

  async presentModal(place: Place) {
    const modal = await this.modalController.create({
      component: ModalComponent,
      componentProps: {
        'place': place
      }
    });
    return await modal.present();
  }

  /*ionViewDidLoad() {
    this.loadMap();
  }*/

  onButtonClick(event){
    
  }

  loadMap() {
    let toTransparent = document.getElementsByClassName('inner-scroll');
    console.log(toTransparent);

    
    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCiElQ5PBeeLSybdXWIGs7c4IWsEvrvuo4',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCiElQ5PBeeLSybdXWIGs7c4IWsEvrvuo4'
    });
    let theLatitude = 42.018620899999995;
    if (this.latitude != null) {
      theLatitude = this.latitude;
    }
    let theLongitude = -4.5315129999999995;
    if (this.longitude != null) {
      theLongitude = this.longitude;
    }

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: theLatitude,
           lng: theLongitude
         },
         zoom: 18,
         tilt: 30
       }
    };
    
    this.map = GoogleMaps.create('map_canvas', mapOptions);
   
    //this.marker = ;

    //this.marker = true;

    this.marker = this.map.addMarkerSync({
      title: 'Current position',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: theLatitude,
        lng: theLongitude
      }
    });

    /*this.marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        //alert('clicked');
    });*/
  }
}


