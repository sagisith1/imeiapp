
export class Option {
    public id: number = 0;
    public name: string = '';
    public description: string = '';

    constructor(jsonData: any = null ) {
        this.parse(jsonData);
    }

    parse(data: any) {
        if (data != null) {
            if (data['id']) { this['id'] = parseInt(data['id'], 0); }
            if (data['name']) { this['name'] = data['name']; }
            if (data['description']) { this['description'] = data['description']; }
        }
    }
}
