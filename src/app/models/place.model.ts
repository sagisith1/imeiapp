import { Option } from './option.model';
import { Circle } from '@ionic-native/google-maps/ngx';


export class Place {
    public id: number = 0;
    public name: string = '';
    public description: string = '';
    public lat: number;
    public lng: number;
    public radius: number;
    public color: string;
    public circle: Circle;
    public options: Option[] = [];
    public isInside: boolean = false;

    constructor(jsonData: any = null ) {
        this.parse(jsonData);
    }

    parse(data: any) {
        if (data != null) {
            this['circle'] = null;
            if (data['id']) { this['id'] = parseInt(data['id'], 0); }
            if (data['name']) { this['name'] = data['name']; }
            if (data['lat']) { this['lat'] = parseFloat(data['lat']); }
            if (data['lng']) { this['lng'] = parseFloat(data['lng']); }
            if (data['radius']) { this['radius'] = parseInt(data['radius'], 0); }
            if (data['color']) { this['color'] = data['color']; }
            if (data['description']) { this['description'] = data['description']; }
            if (data['options']) {
                this.options = [];
                data['options'].forEach( optionElem => {
                    let newOption = new Option(optionElem);
                    this.options.push(newOption);
                });
            }
        }
    }
}




/*


let listAttr = [];
        
            if (typeof data !== 'undefined' &&  data !== null) {
                listAttr = Object.keys(data).map(key => {
                    return key;
                });
            }

            listAttr.forEach( attr => {
                if (data[attr]) {
                    if ( typeof this[attr] === 'number') {
                        this[attr] = parseInt(data[attr], 0);
                    } else {
                        this[attr] = data[attr];
                    }
                }
            });

            */